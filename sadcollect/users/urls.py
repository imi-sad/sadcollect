from django.contrib.auth import views as auth_views
from django.urls import path
from rest_framework.authtoken import views as rest_views
from users.views import get_api_token

app_name = "users"
urlpatterns = [
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="users/login.html"),
        name="login",
    ),
    path("token/", get_api_token, name="get_api_token"),
    path("token-auth/", rest_views.obtain_auth_token),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
]
