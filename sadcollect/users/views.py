from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rest_framework.authtoken.models import Token


@login_required
def get_api_token(request):
    token, _ = Token.objects.get_or_create(user=request.user)
    return render(request, "users/token.html", {"token": token.key})
