from math import log, floor
from django import template
from django.template.defaultfilters import stringfilter
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from datetime import datetime
from urllib.parse import urljoin
import re

register = template.Library()


@register.filter
def human_format(number):
    units = ["", "k", "M", "G", "T", "P"]
    k = 1000.0
    if number <= 0:
        return str(number)
    magnitude = int(floor(log(number, k)))
    return "%.f%s" % (number / k**magnitude, units[magnitude])


def _get_hashtag_url(base_url, hashtag):
    return urljoin(base_url, "search/" + hashtag)


@register.filter(needs_autoescape=True)
@stringfilter
def hashtag_link(text, base_url, autoescape=True):
    if autoescape:
        esc = conditional_escape
    else:
        esc = lambda x: x
    text_with_links = re.sub(
        r"#(\w+)",
        '<a href="' + _get_hashtag_url(base_url, "\\1") + '">#\\1</a>',
        esc(text),
    )
    return mark_safe(text_with_links)


@register.filter
def to_date(date_str):
    return datetime.strptime(date_str, "%a %b %d %H:%M:%S +0000 %Y")


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key, "")
