import json
import uuid
from django.db import models
from polymorphic.models import PolymorphicModel
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone
from django.contrib.auth.models import User


class DataCollection(PolymorphicModel):
    COLLECTION_SOURCE_CHOICE = [("twitter", "Twitter"), ("reddit", "Reddit")]

    COLLECT_TYPE_CHOICES = [("edges", (("coreball", "Coreball"), ("fireball", "Fireball")))]

    name = models.CharField(help_text="Name", max_length=250)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    source = models.CharField(
        max_length=25,
        default="twitter",
        choices=COLLECTION_SOURCE_CHOICE,
        help_text="Collection source",
    )
    depth = models.IntegerField(
        help_text="Number of exploration steps.",
        default=2,
        validators=[MinValueValidator(2), MaxValueValidator(10)],
    )
    sampling_probability = models.IntegerField(
        help_text="Percentage of nodes randomly selected at each layer",
        default=10,
        validators=[MinValueValidator(1), MaxValueValidator(100)],
    )
    max_nodes_per_hop = models.IntegerField(
        help_text="Max number of new nodes at each hop",
        default=1000,
        validators=[MinValueValidator(1), MaxValueValidator(10000)],
    )
    collect_by = models.CharField(
        help_text="Collect by",
        choices=COLLECT_TYPE_CHOICES,
        max_length=25,
        default="coreball",
    )

    # Graph parameters
    minimum_weight = models.IntegerField(
        help_text="Minimum number of RT/mentions needed between 2 users to connect them.",
        default=1,
        validators=[MinValueValidator(1)],
    )
    minimum_degree = models.IntegerField(
        help_text="Minimum number of neighbors a node must have to be included.",
        default=2,
        validators=[MinValueValidator(1)],
    )
    minimum_community_size = models.IntegerField(
        help_text="Communities of users are removed if they contain too few " "elements",
        default=2,
        validators=[MinValueValidator(1)],
    )

    def __str__(self):
        return self.name


class TwitterDataCollection(DataCollection):
    min_mentions = models.IntegerField(
        help_text="Minimum number of times a user must be mentioned by the already collected users to be included.",
        default=0,
        validators=[MinValueValidator(0)],
    )
    max_days = models.IntegerField(
        help_text="Number of days max in the past when collecting a user timeline",
        default=20,
        validators=[MinValueValidator(0), MaxValueValidator(120)],
    )
    max_tweets = models.IntegerField(
        help_text="Maximal number of tweets collected from a user's timeline",
        default=200,
        validators=[MinValueValidator(1), MaxValueValidator(200)],
    )
    popular_tweets = models.IntegerField(
        help_text="Number of popular tweets (for lex. analysis) for each user",
        default=20,
        validators=[MinValueValidator(1), MaxValueValidator(1000)],
    )
    api_version = models.IntegerField(
        help_text="Twitter API version",
        default=1,
        validators=[MinValueValidator(1), MaxValueValidator(2)],
    )


class DataCollectionRun(models.Model):
    COLLECTION_STATUS = [
        ("stopped", "Stopped"),
        ("scheduled", "Scheduled"),
        ("running", "Running"),
        ("paused", "Paused"),
        ("completed", "Completed"),
        ("error", "Error"),
    ]
    id = models.UUIDField(primary_key=True, default=uuid.UUID("f8ed37e7-1a7f-474b-a3dc-35bbb39fa4e0"))

    def js_id(self):
        return str(self.id)[:8].replace("-", "_")

    def __str__(self):
        return str(self.id)

    collection = models.ForeignKey(DataCollection, on_delete=models.CASCADE)
    created = models.DateTimeField(default=timezone.now)
    started = models.DateTimeField(default=timezone.now)
    finished = models.DateTimeField(default=timezone.now)
    status = models.CharField(
        max_length=25,
        choices=COLLECTION_STATUS,
        default="stopped",
        help_text="Collection status",
    )
    log = models.TextField(default="")
    chain = models.JSONField(null=False, default=dict)


class AbstractUser(models.Model):
    collection = models.ForeignKey(DataCollection, on_delete=models.CASCADE)
    name = models.CharField(max_length=250, help_text="Account/User name")

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class InitialNode(AbstractUser):
    class Meta:
        constraints = [models.UniqueConstraint(fields=["collection", "name"], name="unique initial node")]


class UserBlacklist(AbstractUser):
    pass
