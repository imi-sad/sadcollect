from io import StringIO, BytesIO
from base64 import b64decode
import gzip
import logging
import uuid
import networkx as nx
from celery import shared_task
from django.conf import settings
from django.urls import reverse
from django.db.utils import IntegrityError
from django.utils.timezone import localtime
from celery_progress.backend import ProgressRecorder
from spikexplore import graph_explore
from spikexplore.backends.twitter import TwitterNetwork, TwitterCredentials
from spikexplore.config import (
    SamplingConfig,
    GraphConfig,
    DataCollectionConfig,
    TwitterConfig,
)
from collect.models import DataCollection, DataCollectionRun, InitialNode, UserBlacklist
from viewer.models import GraphMetadata
from collect.pysad_helpers import (
    get_date_range,
    graph_postprocessing,
    write_mongodb_output,
)
from collect.sadview_helpers import (
    build_hashtags_list,
    build_cluster_info,
    perform_layout,
)
from collect.lex_helpers import get_tweets_corpus, process_cluster
from collect.exceptions import EmptyGraphCollectedException
from celery.utils.log import get_task_logger


logger = get_task_logger(__name__)
sp_log = logging.getLogger("spikexplore")
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")


class SpikeExploreProgressCallback:
    def __init__(self, progress_recorder):
        self.progress_recorder = progress_recorder
        self.message = "Starting collection..."

    def progress_collect(self, count, total):
        progress_val = 20 + int((count + 1) * 70.0 / float(total))
        self.progress_recorder.set_progress(progress_val, 100, self.message)

    def progress_percent(self, percent, message):
        self.message = message
        self.progress_recorder.set_progress(percent, 100, self.message)


class LexicalProgressCallback:
    def __init__(self, progress_recorder, num_clusters, message):
        self.progress_recorder = progress_recorder
        self.num_clusters = num_clusters
        self.message = message

    def progress_percent(self, count):
        self.progress_recorder.set_progress(60 + int(float(count + 1) * 0.4 / self.num_clusters), 100, self.message)


@shared_task(bind=True)
def graph_collect(self, data_collect_pk):
    progress_recorder = ProgressRecorder(self)
    progress_callback = SpikeExploreProgressCallback(progress_recorder)
    task_log_buffer = StringIO()
    task_log_handler = logging.StreamHandler(task_log_buffer)
    task_log_handler.setFormatter(formatter)
    logger.addHandler(task_log_handler)
    sp_log.addHandler(task_log_handler)
    logger.info("Starting graph collection for config {}".format(data_collect_pk))
    data_collect = DataCollection.objects.get(pk=data_collect_pk)
    assert data_collect.source == "twitter"
    progress_callback.progress_percent(2, "Starting collection")
    # MongoDB cfg
    mongodb_cfg = settings.TWEETS_MONGODB_CFG

    # Create DataRun entry
    data_run = DataCollectionRun()
    data_run.id = graph_collect.request.id
    data_run.collection = data_collect
    data_run.status = "running"
    chained_tasks = {t["task"]: t["options"]["task_id"] for t in graph_collect.request.chain}
    data_run.chain = chained_tasks
    data_run.save()
    initial_accounts = list(InitialNode.objects.filter(collection_id=data_collect).values_list("name", flat=True))
    twitter_creds = TwitterCredentials(
        settings.TWITTER_APP_KEY,
        settings.TWITTER_ACCESS_TOKEN,
        settings.TWITTER_CONSUMER_KEY,
        settings.TWITTER_CONSUMER_SECRET,
    )

    config_collection = DataCollectionConfig(
        exploration_depth=data_collect.depth,
        random_subset_mode="percent",
        random_subset_size=data_collect.sampling_probability,
        expansion_type=data_collect.collect_by,
        degree=2,
        max_nodes_per_hop=data_collect.max_nodes_per_hop,
    )

    # TODO make this configurable
    twitter_config = TwitterConfig(
        min_mentions=data_collect.min_mentions,
        max_day_old=data_collect.max_days,
        max_tweets_per_user=data_collect.max_tweets,
        nb_popular_tweets=data_collect.popular_tweets,
        api_version=data_collect.api_version,
    )

    twitter_config.users_to_remove = list(UserBlacklist.objects.filter(collection_id=data_collect).values_list("name", flat=True))
    twitter_config.users_to_remove += [
        "threader_app",
        "threadreaderapp",
    ]  # backward compatibility
    logger.info("Following nodes are blacklisted: {}".format(twitter_config.users_to_remove))
    twitter_backend = TwitterNetwork(twitter_creds, twitter_config)

    config_graph = GraphConfig(
        min_weight=data_collect.minimum_weight,
        min_degree=data_collect.minimum_degree,
        community_detection=True,
        min_community_size=data_collect.minimum_community_size,
        as_undirected=False,
    )

    sampling_config = SamplingConfig(config_graph, config_collection)
    progress_callback.progress_percent(10, "Collecting tweets...")
    graph, tweets_info = graph_explore.explore(
        twitter_backend,
        initial_accounts,
        sampling_config,
        progress_callback=progress_callback.progress_collect,
    )
    progress_callback.progress_percent(99, "Collect completed.")
    logger.info("Collect completed")
    logger.info("Total number of users mentioned: {}".format(graph.number_of_nodes()))

    start_date, end_date = get_date_range(tweets_info.user_tweets, data_collect.api_version)
    logger.info("Range of tweets date from {} to {}".format(start_date, end_date))

    graph.graph["end_date"] = end_date
    graph.graph["start_date"] = start_date

    ret = graph_postprocessing(graph)
    ret["_id"] = data_run.id
    ret["clusterLex"] = {}  # placeholder for now

    # save tweets
    if tweets_info.user_tweets:
        write_mongodb_output(mongodb_cfg, tweets_info.user_tweets, data_collect.api_version)

    # save metadata
    try:
        gm = GraphMetadata(run=data_run, number_nodes=ret["num_nodes"], number_edges=ret["num_edges"])
        gm.save()
        logger.removeHandler(task_log_handler)
        sp_log.removeHandler(task_log_handler)
        data_run.log = task_log_buffer.getvalue()
        data_run.save()
    except IntegrityError:
        logger.warning("Task {} seem to not exist (deleted ?)".format(data_run))

    return ret


@shared_task(bind=True)
def extract_keywords(self, graph_info):
    try:
        data_run = DataCollectionRun.objects.get(id=graph_info["_id"])
    except DataCollectionRun.DoesNotExist:
        logging.warning("Trying to access a non-existing DataRun (canceled ?")
        return {}
    progress_recorder = ProgressRecorder(self)
    current_log = data_run.log
    task_log_buffer = StringIO()
    task_log_handler = logging.StreamHandler(task_log_buffer)
    task_log_handler.setFormatter(formatter)
    logger.addHandler(task_log_handler)
    logger.info("Starting lexical analysis.")
    progress_recorder.set_progress(10, 100, "Lexical analysis - Retrieving tweets...")
    tweets = get_tweets_corpus(graph_info, 10, progress_recorder=progress_recorder)
    lex_progress = LexicalProgressCallback(progress_recorder, len(tweets.keys()), "Analyzing clusters...")
    res = {}
    cnt = 0
    for k in tweets.keys():
        res[str(k)] = process_cluster(tweets[k])
        lex_progress.progress_percent(cnt)
        cnt += 1

    graph_info["clusterLex"] = res
    logger.info("Lexical analysis completed.")
    progress_recorder.set_progress(99, 100, "Lexical analysis completed.")
    logger.removeHandler(task_log_handler)
    data_run.log = current_log + task_log_buffer.getvalue()
    data_run.save()
    return graph_info


@shared_task(bind=True)
def graph_layout_fa2(self, graph_info):
    try:
        data_run = DataCollectionRun.objects.get(id=graph_info["_id"])
    except DataCollectionRun.DoesNotExist:
        logging.warning("Trying to access a non-existing DataRun (canceled ?")
        return {}
    progress_recorder = ProgressRecorder(self)
    current_log = data_run.log
    task_log_buffer = StringIO()
    task_log_handler = logging.StreamHandler(task_log_buffer)
    task_log_handler.setFormatter(formatter)
    logger.addHandler(task_log_handler)
    logger.info("Starting layout.")
    progress_recorder.set_progress(5, 100, "Layout - Computing attributes...")
    MIN_NODE_SIZE = 3
    MAX_NODE_SIZE = 15
    palette = [
        "#a6cee3",
        "#1f78b4",
        "#b2df8a",
        "#33a02c",
        "#fb9a99",
        "#e31a1c",
        "#fdbf6f",
        "#ff7f00",
        "#cab2d6",
        "#6a3d9a",
        "#ffff99",
        "#b15928",
    ]
    # read graph
    with BytesIO(b64decode(graph_info["graph"])) as f:
        with gzip.GzipFile(mode="r", fileobj=f) as zf:
            graph_str = zf.read().decode("utf-8").replace("\x03", "")  # XXX replace weird chars in twitter profile ?!
        with StringIO(graph_str) as sf:
            graph = nx.read_gexf(sf)

    if nx.is_empty(graph):  #  Empty graph -> return
        logger.info("Empty graph -> skipping layout step")
        graph_info["graphLayout"] = {}
        logger.removeHandler(task_log_handler)
        data_run.log = current_log + task_log_buffer.getvalue()
        data_run.status = "completed"
        data_run.finished = localtime()
        data_run.save()
        return graph_info

    # compute attributes used in viewer
    degree = dict(nx.degree(graph))
    in_degree = dict(graph.in_degree())
    out_degree = dict(graph.out_degree())
    num_communities = max(graph_info["clusters"].values())
    edges_weight = nx.get_edge_attributes(graph, "weight")
    max_weight = max(edges_weight.values())
    max_hop = max(nx.get_node_attributes(graph, "spikyball_hop").values())
    max_degree = max(degree.values())

    # node attributes
    node_attr = {
        n: {
            "color": palette[graph_info["clusters"][n] % len(palette)],
            "size": MIN_NODE_SIZE + (degree[n] * (MAX_NODE_SIZE - MIN_NODE_SIZE)) / max_degree,
            "zIndex": 0,
        }
        for n in graph.nodes()
    }

    nx.set_node_attributes(graph, node_attr)
    nx.set_node_attributes(graph, in_degree, "inDegree")
    nx.set_node_attributes(graph, out_degree, "outDegree")
    nx.set_node_attributes(graph, degree, "degree")

    # edge attributes
    edge_attr = {
        e: {
            "color": "#cccccc{:x}".format(int(20 + 235.0 * edges_weight[e] / max_weight)),
            "size": int(edges_weight[e] * 5.0 / max_weight + 1),
            "zIndex": 0,
        }
        for e in graph.edges()
    }
    nx.set_edge_attributes(graph, edge_attr)

    hashtags = build_hashtags_list(graph)
    cluster_info = build_cluster_info(graph, graph_info["clusters"], graph_info["clusterLex"])
    graph_attr = {
        "num communities": num_communities,
        "maxWeight": max_weight,
        "maxDegree": max_degree,
        "hashtags": hashtags,
        "maxHop": max_hop,
    }
    logger.info("Graph attributes computed. Starting forcealtas2...")
    progress_recorder.set_progress(20, 100, "Layout - forceatlas2...")
    graph_layout_str = perform_layout(graph)
    graph_info["graphLayout"] = {
        "attributes": graph_attr,
        "clusterInfo": cluster_info,
        "compressedGraph": graph_layout_str,
        "isGexf": True,
    }
    logger.info("Layout completed.")
    logger.removeHandler(task_log_handler)
    data_run.log = current_log + task_log_buffer.getvalue()
    data_run.status = "completed"
    data_run.finished = localtime()
    data_run.save()
    return graph_info


@shared_task
def error_handler(request, exc, traceback):
    err_txt = "Task {0} raised exception: {1}\n{2}".format(request.id, exc, traceback)
    logger.error(err_txt)

    logger.info("Task {} root {}".format(request.id, request.root_id))
    try:
        data_run = DataCollectionRun.objects.get(id=request.root_id)
        data_run.log += err_txt
        data_run.status = "error"
        data_run.finished = localtime()
        data_run.save()
    except DataCollectionRun.DoesNotExist:
        logger.warning("Task {} not found in runs.".format(request.root_id))
