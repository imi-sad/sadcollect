# Generated by Django 2.2.17 on 2021-02-03 14:23

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("collect", "0003_credentials"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Credentials",
            new_name="Credential",
        ),
    ]
