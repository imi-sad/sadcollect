# Generated by Django 2.2.17 on 2021-04-16 13:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("collect", "0005_datacollectionrun_token"),
    ]

    operations = [
        migrations.AlterField(
            model_name="datacollection",
            name="collect_by",
            field=models.CharField(
                choices=[("edges", (("coreball", "Coreball"), ("fireball", "Fireball")))],
                default="coreball",
                help_text="Collect by",
                max_length=25,
            ),
        ),
        migrations.DeleteModel(
            name="Credential",
        ),
    ]
