from django.contrib import admin
from .models import DataCollectionRun, InitialNode
from .models import TwitterDataCollection


class InitialNodeAdmin(admin.TabularInline):
    model = InitialNode


class TwitterDataCollectionAdmin(admin.ModelAdmin):
    model = TwitterDataCollection


class DataCollectionRunAdmin(admin.ModelAdmin):
    model = DataCollectionRun


admin.site.register(TwitterDataCollection, TwitterDataCollectionAdmin)
admin.site.register(DataCollectionRun, DataCollectionRunAdmin)
