from datetime import datetime
from base64 import b64encode
import io
import gzip
import logging
import networkx as nx
from pymongo import MongoClient
from urllib.parse import quote_plus
from pymongo.errors import ConnectionFailure, BulkWriteError

logger = logging.getLogger()


def get_date_range(tweets, api_version):
    assert api_version == 1 or api_version == 2
    if not tweets:
        return None, None
    format_str = "%a %b %d %H:%M:%S +0000 %Y" if api_version == 1 else "%Y-%m-%dT%H:%M:%S.%fZ"
    kmax = max(
        tweets.keys(),
        key=(lambda k: datetime.strptime(tweets[k]["created_at"], format_str)),
    )
    kmin = min(
        tweets.keys(),
        key=(lambda k: datetime.strptime(tweets[k]["created_at"], format_str)),
    )
    return tweets[kmin]["created_at"], tweets[kmax]["created_at"]


def graph_postprocessing(g):
    clusters = nx.get_node_attributes(g, "community")
    users = list(g.nodes())
    max_tweet_id = nx.get_node_attributes(g, "max_tweet_id")

    with io.BytesIO() as f:
        with gzip.GzipFile(mode="w", fileobj=f) as zf:
            nx.write_gexf(g, zf, prettyprint=False)
        return {
            "graph": b64encode(f.getvalue()).decode("ascii"),
            "clusters": clusters,
            "users": users,
            "max_tweet_id": max_tweet_id,
            "num_nodes": g.number_of_nodes(),
            "num_edges": g.number_of_edges(),
        }


def write_mongodb_output(cfg, tweets, api_version):
    assert api_version == 1 or api_version == 2
    format_str = "%a %b %d %H:%M:%S +0000 %Y" if api_version == 1 else "%Y-%m-%dT%H:%M:%S.%fZ"
    mongo_url = "mongodb://{}:{}@{}/{}?authSource={}".format(
        quote_plus(cfg["user"]),
        quote_plus(cfg["password"]),
        cfg["host"],
        cfg["dbname"],
        cfg["dbname"],
    )
    try:
        client = MongoClient(mongo_url, cfg["port"])
        coll = client[cfg["dbname"]][cfg["tweets_collection"]]
        # add _id field and date ('created_at' is a string)
        tweets_mongo = [
            {
                **v,
                "_id": k,
                "created_date": datetime.strptime(v["created_at"], format_str),
            }
            for k, v in tweets.items()
        ]

        coll.insert_many(tweets_mongo, ordered=False)
    except BulkWriteError:
        logger.info("Duplicates were found.")
    except ConnectionFailure:
        logger.error("Server not available {}@{}:{}".format(cfg["user"], cfg["host"], cfg["port"]))
