from rest_framework import serializers
from collect import models as collect_models
from viewer import models as viewer_models


class GraphMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = viewer_models.GraphMetadata
        fields = ["number_nodes", "number_edges", "info", "name"]


class DataCollectionRunDetailSerializer(serializers.ModelSerializer):
    graphmetadata = GraphMetadataSerializer(read_only=True)

    class Meta:
        model = collect_models.DataCollectionRun
        fields = ["id", "status", "finished", "graphmetadata"]


class DataCollectionRunSerializer(serializers.ModelSerializer):
    class Meta:
        model = collect_models.DataCollectionRun
        fields = ["id", "status", "finished"]


class TwitterCollectionSummarySerializer(serializers.ModelSerializer):
    num_initial_nodes = serializers.IntegerField()
    num_blocked_nodes = serializers.IntegerField()
    num_runs = serializers.IntegerField()

    class Meta:
        model = collect_models.TwitterDataCollection
        fields = [
            "id",
            "name",
            "depth",
            "sampling_probability",
            "num_initial_nodes",
            "num_blocked_nodes",
            "num_runs",
        ]


class TwitterCollectionSerializer(serializers.ModelSerializer):
    initialnode_set = serializers.StringRelatedField(many=True)
    userblacklist_set = serializers.StringRelatedField(many=True)
    datacollectionrun_set = DataCollectionRunSerializer(many=True, read_only=True)

    class Meta:
        model = collect_models.TwitterDataCollection
        fields = [
            "id",
            "name",
            "depth",
            "sampling_probability",
            "initialnode_set",
            "userblacklist_set",
            "datacollectionrun_set",
        ]
