import json
from django import forms
from django.forms import ModelForm, Form
from django.forms import HiddenInput
from .models import DataCollection, TwitterDataCollection
from django_select2.forms import Select2Widget, Select2TagWidget


class DataCollectionFormUpdate(ModelForm):
    source = forms.CharField(widget=forms.TextInput(attrs={"readonly": "readonly"}))

    class Meta:
        model = DataCollection
        fields = (
            "name",
            "source",
            "depth",
            "sampling_probability",
            "collect_by",
            "max_nodes_per_hop",
        )


class TwitterConfigurationForm(ModelForm):
    class Meta:
        model = TwitterDataCollection
        fields = ("min_mentions", "max_days", "max_tweets", "popular_tweets")


class HashtagSearch(Form):
    hashtag = forms.CharField(max_length=50, min_length=3, widget=Select2Widget)


class UsersAdd(Form):
    def __init__(self, *args, **kwargs):
        users = kwargs.pop("users")
        super(UsersAdd, self).__init__(*args, **kwargs)
        self.fields["users_list"] = forms.MultipleChoiceField(widget=Select2TagWidget, choices=users, required=False)
        # pass the list of choices allowed as hidden input s.t. form can be validated as POST time
        self.fields["users_choices"] = forms.CharField(widget=HiddenInput(), initial=json.dumps(users))
