from django.urls import path, re_path, include
from rest_framework import routers
import collect.views as views

app_name = "collect"
router = routers.SimpleRouter()
router.register(r"collection", views.TwitterCollectViewSet, basename="collection")
router.register(r"run", views.DataCollectionRunViewSet, basename="run")

urlpatterns = [
    path("", views.home, name="home"),
    path("create/", views.CollectCreateView.as_view(), name="create_collect"),
    path("update/<int:pk>", views.CollectUpdateView.as_view(), name="update_collect"),
    path("delete/<int:pk>", views.CollectDeleteView.as_view(), name="delete_collect"),
    path("start-collect/<int:collect_id>", views.start_collect, name="start_collect"),
    path("initial-nodes/<int:collect_id>/", views.home_nodes, name="collect_start_nodes"),
    path(
        "initial-nodes/<int:collect_id>/search/<str:hashtag>",
        views.home_nodes,
        name="collect_start_nodes",
    ),
    path(
        "initial-nodes/<int:collect_id>/add",
        views.NodeCreateView.as_view(),
        name="add_node",
    ),
    path(
        "initial-nodes/<int:collect_id>/add-multi/<str:hashtag>",
        views.nodes_multi_add,
        name="nodes_multi_add",
    ),
    path(
        "initial-nodes/<int:collect_id>/delete/<int:pk>",
        views.NodeDeleteView.as_view(),
        name="delete_node",
    ),
    path(
        "initial-nodes/<int:collect_id>/update/<int:pk>",
        views.NodeUpdateView.as_view(),
        name="update_node",
    ),
    path(
        "blacklist-nodes/<int:collect_id>",
        views.blacklist_nodes,
        name="blacklist_nodes",
    ),
    path(
        "blacklist-nodes/<int:collect_id>/add",
        views.BlacklistCreateView.as_view(),
        name="add_blacklist_node",
    ),
    path(
        "blacklist-nodes/<int:collect_id>/delete/<int:pk>",
        views.BlacklistDeleteView.as_view(),
        name="delete_blacklist_node",
    ),
    path(
        "blacklist-nodes/<int:collect_id>/update/<int:pk>",
        views.BlacklistUpdateView.as_view(),
        name="update_blacklist_node",
    ),
    path("<int:collect_id>/", views.list_runs, name="list_runs"),
    path(
        "<int:collect_id>/<uuid:collect_run_id>/",
        views.detail_collect_run,
        name="detail_collect_run",
    ),
    path(
        "<int:collect_id>/delete/<uuid:collect_run_id>/",
        views.RunDeleteView.as_view(),
        name="delete_run",
    ),
    path("log/<uuid:collect_run_id>", views.run_log, name="run_log"),
    path(
        "<int:collect_id>/annotate/<uuid:collect_run_id>",
        views.AnnotateRunView.as_view(),
        name="annotate_run",
    ),
    path(
        "<int:collect_id>/rename/<uuid:collect_run_id>",
        views.RenameRunView.as_view(),
        name="rename_run",
    ),
    path("hashtag/<str:hashtag>", views.display_hashtag, name="display_hashtag"),
    re_path(
        r"^collect-progress/(?P<task_id>[\w-]+)/$",
        views.get_progress,
        name="task_status",
    ),
    path("api/", include((router.urls, "collect"))),
]
