import pymongo
from urllib.parse import quote_plus
from django.conf import settings
from pymongo import MongoClient
import re
from collections import defaultdict
from celery.utils.log import get_task_logger
import nltk
from nltk.tokenize import RegexpTokenizer
import requests

nltk.download("stopwords")
from nltk.corpus import stopwords
import yake


logger = get_task_logger("collect.tasks")


def get_tweets_corpus(
    graph_info,
    max_tweets_per_user,
    remove_url=True,
    sort_langs=True,
    clean_mentions="remove",
    progress_recorder=None,
):
    """
    @param graph_info: the output of graph collection task.
    @type graph_info: dictionary containing a C{clusters} dictionary, a C{users} list and a C{max_tweet_info} dictionary.
    @param max_tweets_per_user: Retrieve at most a number of tweets for each user
    @type max_tweets_per_user: int
    @param remove_url: flag to toggle URL removal in tweets
    @type remove_url: bool
    @param sort_langs: if true, output dictionary has one subkey per language (per cluster)
    @type sort_langs: bool
    @param clean_mentions: if set to C{remove}, mentions are removed, if set to C{prefix} they are prefixed with C{_mention_}
    @type clean_mentions: str
    @return: Dictionary of tweets. Keys = cluster_id, subkeys = lang (depending on C{sort_langs} value)
    @rtype: dict
    """
    # open mongodb connection and get tweets
    cfg_mongo = settings.TWEETS_MONGODB_CFG
    mongo_url = "mongodb://{}:{}@{}/default_db?authSource={}".format(
        quote_plus(cfg_mongo["user"]),
        quote_plus(cfg_mongo["password"]),
        cfg_mongo["host"],
        cfg_mongo["dbname"],
    )
    client = MongoClient(mongo_url, cfg_mongo["port"])
    coll = client[cfg_mongo["dbname"]][cfg_mongo["tweets_collection"]]

    clean_url_regex = re.compile(r"https?:\/\/.*[\r\n]*")
    clean_mentions_regex = re.compile(r"(^|\s)@(\w+)")
    tweets_dic = defaultdict(lambda: defaultdict(list)) if sort_langs else defaultdict(list)
    tw_count = 0
    logger.info("Retrieving tweets...")

    cnt = 0
    for name in graph_info["users"]:
        cnt += 1
        cluster = graph_info["clusters"].get(name, -1)
        if cluster < 0:
            continue
        # get n most recent tweets from user
        query = {"user.screen_name": name}
        if name in graph_info["max_tweet_id"]:  # we have the latest tweet id at collection time -> use it
            query["id"] = {"$lte": graph_info["max_tweet_id"][name]}

        tweets = list(coll.find(query).sort("id", pymongo.DESCENDING).limit(max_tweets_per_user))
        if progress_recorder:
            progress_recorder.set_progress(
                float(cnt) * 50 / len(graph_info["users"]) + 10,
                100,
                "Generating corpus",
            )
        for t in tweets:
            if "retweeted_status" in t:
                txt = t["retweeted_status"]["full_text"]
            else:
                txt = t["full_text"]
            if remove_url:
                txt = clean_url_regex.sub(" ", txt)
            if clean_mentions == "prefix":
                txt = clean_mentions_regex.sub(r" _mention_\2_", txt)
            elif clean_mentions == "remove":
                txt = clean_mentions_regex.sub(" ", txt)
            if sort_langs:
                tweets_dic[cluster][t["lang"]].append(txt)
            else:
                tweets_dic[cluster].append(txt)
        if progress_recorder:
            progress_recorder.set_progress(
                int(float(cnt) * 50 / len(graph_info["users"])) + 10,
                100,
                "Generating corpus",
            )
        tw_count += len(tweets)
    logger.info("{} tweets retrieved.".format(tw_count))
    return tweets_dic


# get language with most entries in cluster dict
def get_dominant_lang(twdic):
    lang_stats = {k: len(twdic[k]) for k in twdic.keys()}
    return max(lang_stats, key=lang_stats.get)


def process_cluster(twdic, ngram_size=1, num_keywords=20):
    # long format of languages for stopword identification

    languages_long = {
        "ar": "arabic",
        "en": "english",
        "de": "german",
        "fr": "french",
        "it": "italian",
        "es": "spanish",
        "pt": "portuguese",
        "el": "greek",
        "tr": "turkish",
        "ro": "romanian",
        "nl": "dutch",
        "sv": "swedish",
        "no": "norwegian",
        "ru": "russian",
        "fi": "finnish",
        "da": "danish",
        "hu": "hungarian",
        "ro": "romanian",
    }

    maj_lang = get_dominant_lang(twdic)
    if maj_lang not in languages_long.keys():
        maj_lang = "en"

    logger.info("Extracting keywords from cluster maj. language {} with {} tweets.".format(languages_long[maj_lang], len(twdic[maj_lang])))
    stopwords_lang = set(stopwords.words(languages_long.get(maj_lang, "en"))).union(["amp"])  # tokenizer leave remnants of &amp;
    tokenizer = RegexpTokenizer(r"\w+")
    texts_split = [tokenizer.tokenize(text.lower()) for text in twdic[maj_lang]]
    text_sum = sum(texts_split, [])
    text_stop = [f for f in text_sum if f not in stopwords_lang]
    kw_ext = yake.KeywordExtractor(lan=maj_lang, n=ngram_size, top=num_keywords)
    res = kw_ext.extract_keywords(" ".join(text_stop))
    return {r[0]: (r[1], text_stop.count(r[0])) for r in res}
