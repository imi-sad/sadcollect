import json
from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Count
from django.contrib import messages
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.views.generic import CreateView, UpdateView, DeleteView
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy, reverse
from django.contrib.auth.decorators import login_required
from rest_framework import viewsets
from rest_framework.response import Response
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from crispy_forms.bootstrap import Accordion, AccordionGroup
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from celery import chain
from twython import Twython
from collect.forms import HashtagSearch, UsersAdd
from collect.models import (
    TwitterDataCollection,
    DataCollection,
    InitialNode,
    DataCollectionRun,
    UserBlacklist,
)
from collect.tasks import (
    graph_collect,
    graph_layout_fa2,
    extract_keywords,
    error_handler,
)
from collect.serializers import (
    TwitterCollectionSerializer,
    TwitterCollectionSummarySerializer,
    DataCollectionRunDetailSerializer,
)
from viewer.models import GraphMetadata

from django.http import HttpResponse
from celery.result import AsyncResult
from celery_progress.backend import Progress
from app.celery import app


@login_required
def get_progress(request, task_id):
    # this function is a copy of its equivalent in celery progress, except
    # it does not serialize the full result object (uuid triggers an exception in json.dumps)
    res = AsyncResult(task_id)
    progress = Progress(res)
    info = progress.get_info()
    info_clean = {
        "complete": info["complete"],
        "progress": info["progress"],
        "state": info["state"],
        "success": info["success"],
    }
    if "result" in info:
        if info["state"] == "SUCCESS":
            info_clean["result"] = {"_id": info["result"]["_id"]}
        else:
            info_clean["result"] = info["result"]
    return HttpResponse(json.dumps(info_clean), content_type="application/json")


@login_required
def home(request):
    lst_collects = DataCollection.objects.filter(owner=request.user).annotate(nb_nodes=Count("initialnode"))
    return render(request, "collect/collect.html", {"lst_collect": lst_collects})


@login_required
def list_runs(request, collect_id):
    collect = get_object_or_404(DataCollection, id=collect_id)
    if collect.owner != request.user:
        raise PermissionDenied()
    lst_collects = DataCollection.objects.filter(owner=request.user).annotate(nb_nodes=Count("initialnode"))
    datacollectruns = DataCollectionRun.objects.filter(collection_id=collect)
    return render(
        request,
        "collect/collect.html",
        {
            "collect": collect,
            "lst_collect": lst_collects,
            "datacollectruns": datacollectruns,
        },
    )


@login_required
def home_nodes(request, collect_id, hashtag=None):
    collect = get_object_or_404(DataCollection, id=collect_id)
    if collect.owner != request.user:
        raise PermissionDenied()
    lst_nodes = InitialNode.objects.filter(collection_id=collect_id)

    mentions = []
    hashtags = []
    api_result = None

    if request.method == "POST":
        hashtag_form = HashtagSearch(request.POST)
    else:
        hashtag_form = HashtagSearch()
    if request.method == "POST" and hashtag_form.is_valid():
        hashtag = hashtag_form.cleaned_data["hashtag"]
        if hashtag[0] == "#":  # remove leading '#' as the query takes care of it
            hashtag = hashtag[1:]
    if hashtag:
        api = Twython(settings.TWITTER_APP_KEY, access_token=settings.TWITTER_ACCESS_TOKEN)
        api_result = api.search(q="#" + hashtag, count=25, result_type="mixed")
        # find all hashtags in tweets retrieved
        hashtags_list = set(
            sum(
                [[e["text"] for e in api_result["statuses"][k]["entities"]["hashtags"]] for k in range(len(api_result["statuses"]))],
                [],
            )
        )
        hashtags = [{"id": h, "text": "#" + h} for h in hashtags_list]
        # find all mentions in tweets retrieved
        mention_list = set(
            sum(
                [[e["screen_name"] for e in api_result["statuses"][k]["entities"]["user_mentions"]] for k in range(len(api_result["statuses"]))],
                [],
            )
        )
        # add users
        users = set([api_result["statuses"][k]["user"]["screen_name"] for k in range(len(api_result["statuses"]))]).union(mention_list)
        mentions = [{"id": m, "text": "@" + m} for m in users]
        mentions_select = [(m["id"], m["text"]) for m in mentions]
        users_form = UsersAdd(users=mentions_select)
    else:
        users_form = UsersAdd(users=())
    return render(
        request,
        "collect/initial_nodes.html",
        {
            "lst_nodes": lst_nodes,
            "collect_id": collect_id,
            "hashtag_data": api_result,
            "hashtag_form": hashtag_form,
            "hashtag": hashtag,
            "users_form": users_form,
            "hashtags_select2": json.dumps(hashtags),
            "has_hashtags": len(hashtags) > 0,
            "mentions_select2": json.dumps(mentions),
            "has_mentions": len(mentions) > 0,
            "mode": "initial_nodes",
        },
    )


@login_required
def nodes_multi_add(request, collect_id, hashtag=None):
    collect = get_object_or_404(DataCollection, id=collect_id)
    if collect.owner != request.user:
        raise PermissionDenied()
    users_form = UsersAdd(request.POST, users=json.loads(request.POST.get("users_choices", "[]")))
    if users_form.is_valid():
        for u in users_form.cleaned_data["users_list"]:
            # do not create duplicates
            _, _ = InitialNode.objects.get_or_create(name=u, collection=collect)

    return redirect("collect:collect_start_nodes", collect_id=collect_id, hashtag=hashtag)


@login_required
def blacklist_nodes(request, collect_id):
    collect = get_object_or_404(DataCollection, id=collect_id)
    if collect.owner != request.user:
        raise PermissionDenied()
    lst_nodes = UserBlacklist.objects.filter(collection_id=collect_id)
    return render(
        request,
        "collect/blacklisted_nodes.html",
        {"lst_nodes": lst_nodes, "collect_id": collect_id, "mode": "blacklist_nodes"},
    )


@login_required
def detail_collect_run(request, collect_id, collect_run_id):
    return render(request, "detail_collect_run.html")


def _configure_collection_form(form):
    form.helper = FormHelper()
    form.helper.form_tag = False
    form.helper.layout = Layout(
        Accordion(
            AccordionGroup(
                "Collection parameters",
                "name",
                "depth",
                "sampling_probability",
                "collect_by",
                "max_nodes_per_hop",
            ),
            AccordionGroup(
                "Graph parameters",
                "minimum_weight",
                "minimum_degree",
                "minimum_community_size",
            ),
            AccordionGroup(
                "Twitter parameters",
                "api_version",
                "min_mentions",
                "max_days",
                "max_tweets",
                "popular_tweets",
            ),
        )
    )
    return form


@method_decorator(login_required, name="dispatch")
class CollectCreateView(CreateView):
    template_name = "edit.html"
    model = TwitterDataCollection
    success_message = "Success"
    fields = (
        "name",
        "depth",
        "sampling_probability",
        "collect_by",
        "max_nodes_per_hop",
        "minimum_weight",
        "minimum_degree",
        "minimum_community_size",
        "api_version",
        "min_mentions",
        "max_days",
        "max_tweets",
        "popular_tweets",
    )

    def get_context_data(self, **kwargs):
        context = super(CollectCreateView, self).get_context_data(**kwargs)
        context["title"] = "Create new data collection"
        context["action"] = "Create"
        return context

    def get_form(self, form_class=None):
        form = super(CollectCreateView, self).get_form(form_class)
        return _configure_collection_form(form)

    def form_valid(self, form):
        owner = self.request.user
        form.instance.owner = owner
        collect = form.save(commit=False)
        collect.save()
        return redirect("collect:collect_start_nodes", collect.id)

    def get_success_url(self):
        return reverse_lazy("collect:home")


def _configure_node_form(form):
    form.helper = FormHelper()
    form.helper.form_tag = False
    return form


@method_decorator(login_required, name="dispatch")
class NodeCreateView(CreateView):
    template_name = "edit.html"
    success_message = "Success"
    model = InitialNode
    fields = ("name",)

    def get_form(self, form_class=None):
        form = super(NodeCreateView, self).get_form(form_class)
        return _configure_node_form(form)

    def get_context_data(self, **kwargs):
        context = super(NodeCreateView, self).get_context_data(**kwargs)
        context["title"] = "Add node"
        context["action"] = "Create"
        return context

    def form_valid(self, form):
        collect_id = self.kwargs["collect_id"]
        collect = DataCollection.objects.get(id=collect_id)
        form.instance.collection = collect
        return super(NodeCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            "collect:collect_start_nodes",
            kwargs={"collect_id": self.kwargs["collect_id"]},
        )


@method_decorator(login_required, name="dispatch")
class BlacklistCreateView(CreateView):
    template_name = "edit.html"
    success_message = "Success"
    model = UserBlacklist
    fields = ("name",)

    def get_form(self, form_class=None):
        form = super(BlacklistCreateView, self).get_form(form_class)
        return _configure_node_form(form)

    def get_context_data(self, **kwargs):
        context = super(BlacklistCreateView, self).get_context_data(**kwargs)
        context["title"] = "Add blacklisted node"
        context["action"] = "Create"
        return context

    def form_valid(self, form):
        collect_id = self.kwargs["collect_id"]
        collect = DataCollection.objects.get(id=collect_id)
        form.instance.collection = collect
        return super(BlacklistCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy("collect:blacklist_nodes", kwargs={"collect_id": self.kwargs["collect_id"]})


@method_decorator(login_required, name="dispatch")
class CollectUpdateView(UpdateView):
    model = TwitterDataCollection
    template_name = "edit.html"
    # form_class = DataCollectionFormUpdate
    success_message = "Success: Data collection parameters updated."
    success_url = reverse_lazy("collect:home")
    fields = (
        "name",
        "depth",
        "sampling_probability",
        "collect_by",
        "max_nodes_per_hop",
        "minimum_weight",
        "minimum_degree",
        "minimum_community_size",
        "api_version",
        "min_mentions",
        "max_days",
        "max_tweets",
        "popular_tweets",
    )

    def get_form(self, form_class=None):
        form = super(CollectUpdateView, self).get_form(form_class)
        return _configure_collection_form(form)

    def get_context_data(self, **kwargs):
        context = super(CollectUpdateView, self).get_context_data(**kwargs)
        context["title"] = "Update collection"
        context["action"] = "Update"
        return context

    def get_object(self, queryset=None):
        """
        Check the logged in user is the owner of the object or 404
        """
        obj = super(CollectUpdateView, self).get_object(queryset)
        if obj.owner != self.request.user:
            raise PermissionDenied()
        return obj


@method_decorator(login_required, name="dispatch")
class NodeUpdateView(UpdateView):
    model = InitialNode
    template_name = "edit.html"
    success_message = "Success: Node updated."
    fields = ("name",)

    def get_form(self, form_class=None):
        form = super(NodeUpdateView, self).get_form(form_class)
        return _configure_node_form(form)

    def get_context_data(self, **kwargs):
        context = super(NodeUpdateView, self).get_context_data(**kwargs)
        context["title"] = "Update node"
        context["action"] = "Update"
        return context

    def get_object(self, queryset=None):
        """
        Check the logged in user is the owner of the object or 404
        """
        obj = super(NodeUpdateView, self).get_object(queryset)
        collect = get_object_or_404(DataCollection, id=self.kwargs["collect_id"])
        if collect.owner != self.request.user:
            raise PermissionDenied()
        return obj

    def get_success_url(self, **kwargs):
        return reverse_lazy(
            "collect:collect_start_nodes",
            kwargs={"collect_id": self.kwargs["collect_id"]},
        )


@method_decorator(login_required, name="dispatch")
class BlacklistUpdateView(UpdateView):
    model = UserBlacklist
    template_name = "edit.html"
    success_message = "Success: Blacklist updated."
    fields = ("name",)

    def get_form(self, form_class=None):
        form = super(BlacklistUpdateView, self).get_form(form_class)
        return _configure_node_form(form)

    def get_context_data(self, **kwargs):
        context = super(BlacklistUpdateView, self).get_context_data(**kwargs)
        context["title"] = "Update blacklisted node"
        context["action"] = "Update"
        return context

    def get_object(self, queryset=None):
        """
        Check the logged in user is the owner of the object or 404
        """
        obj = super(BlacklistUpdateView, self).get_object(queryset)
        collect = get_object_or_404(DataCollection, id=self.kwargs["collect_id"])
        if collect.owner != self.request.user:
            raise PermissionDenied()
        return obj

    def get_success_url(self, **kwargs):
        return reverse_lazy("collect:blacklist_nodes", kwargs={"collect_id": self.kwargs["collect_id"]})


@method_decorator(login_required, name="dispatch")
class CollectDeleteView(DeleteView):
    model = DataCollection
    template_name = "delete.html"
    success_message = "Success: Collection was deleted."
    success_url = reverse_lazy("collect:home")

    def get_context_data(self, **kwargs):
        context = super(CollectDeleteView, self).get_context_data(**kwargs)
        context["title"] = "Delete collection - {}".format(kwargs["object"].name)
        context["item"] = "configuration"
        return context

    def get_object(self, queryset=None):
        """
        Check the logged in user is the owner of the object or 404
        """
        obj = super(CollectDeleteView, self).get_object(queryset)
        if obj.owner != self.request.user:
            raise PermissionDenied()
        return obj


@method_decorator(login_required, name="dispatch")
class NodeDeleteView(DeleteView):
    model = InitialNode
    template_name = "delete.html"
    success_message = "Success: Start node was deleted."

    def get_context_data(self, **kwargs):
        context = super(NodeDeleteView, self).get_context_data(**kwargs)
        context["title"] = "Delete node - {}".format(kwargs["object"].name)
        context["item"] = "node"
        return context

    def get_object(self, queryset=None):
        """
        Check the logged in user is the owner of the object or 404
        """
        obj = super(NodeDeleteView, self).get_object(queryset)
        collect = get_object_or_404(DataCollection, id=self.kwargs["collect_id"])
        if collect.owner != self.request.user:
            raise PermissionDenied()
        return obj

    def get_success_url(self, **kwargs):
        return reverse_lazy(
            "collect:collect_start_nodes",
            kwargs={"collect_id": self.kwargs["collect_id"]},
        )


@method_decorator(login_required, name="dispatch")
class BlacklistDeleteView(DeleteView):
    model = UserBlacklist
    template_name = "delete.html"
    success_message = "Success: Blacklisted user was deleted."

    def get_context_data(self, **kwargs):
        context = super(BlacklistDeleteView, self).get_context_data(**kwargs)
        context["title"] = "Delete node - {} - from blacklist".format(kwargs["object"].name)
        context["item"] = "node"
        return context

    def get_object(self, queryset=None):
        """
        Check the logged in user is the owner of the object or 404
        """
        obj = super(BlacklistDeleteView, self).get_object(queryset)
        collect = get_object_or_404(DataCollection, id=self.kwargs["collect_id"])
        if collect.owner != self.request.user:
            raise PermissionDenied()
        return obj

    def get_success_url(self, **kwargs):
        return reverse_lazy("collect:blacklist_nodes", kwargs={"collect_id": self.kwargs["collect_id"]})


@login_required
def start_collect(request, collect_id):
    collect = get_object_or_404(DataCollection, id=collect_id)
    if collect.owner != request.user:
        raise PermissionDenied()

    ret = chain(graph_collect.s(collect_id), extract_keywords.s(), graph_layout_fa2.s()).apply_async(link_error=error_handler.s())

    return redirect("collect:list_runs", collect_id=collect.id)


@method_decorator(login_required, name="dispatch")
class RunDeleteView(DeleteView):
    model = DataCollectionRun
    template_name = "delete.html"
    success_message = "Success: Run was deleted."
    success_url = reverse_lazy("collect:home")
    pk_url_kwarg = "collect_run_id"

    def cancel_tasks(self, chain_tasks):
        for t in chain_tasks.values():
            app.control.revoke(t)

    def get_context_data(self, **kwargs):
        context = super(RunDeleteView, self).get_context_data(**kwargs)
        context["title"] = "Delete run - {}".format(kwargs["object"].pk)
        context["item"] = "run"
        return context

    def dispatch(self, request, *args, **kwargs):
        collect = DataCollection.objects.get(id=kwargs["collect_id"])
        if collect.owner != self.request.user:
            raise PermissionDenied()
        return super(RunDeleteView, self).dispatch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        docs_delete = []
        task_mongo_cfg = settings.CELERY_MONGODB_BACKEND_SETTINGS
        mongo_url = settings.CELERY_RESULT_BACKEND
        obj = self.get_object()
        cur_doc_id = str(obj.pk)
        if obj.status == "running":
            self.cancel_tasks(obj.chain)

        try:
            client = MongoClient(mongo_url)
            collection = client[task_mongo_cfg["database"]][task_mongo_cfg["taskmeta_collection"]]
            while True:
                docs_delete.append(cur_doc_id)
                doc = collection.find_one({"_id": cur_doc_id})
                if doc is not None and "children" in doc:
                    try:
                        cur_doc_id = doc["children"][0][0][0]
                    except IndexError:
                        break
                else:
                    break
            for doc_id in docs_delete:
                collection.delete_one({"_id": doc_id})

        except ConnectionFailure:
            messages.error("Error connecting to MongoDB")
        return super(RunDeleteView, self).delete(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        return reverse_lazy("collect:list_runs", kwargs={"collect_id": self.kwargs["collect_id"]})


@login_required
def run_log(request, collect_run_id):
    collect_run = get_object_or_404(DataCollectionRun, id=collect_run_id)
    collect = get_object_or_404(DataCollection, id=collect_run.collection.id)
    if collect.owner != request.user:
        raise PermissionDenied()
    return render(request, "collect/log.html", {"run": collect_run})


@login_required
def display_hashtag(request, hashtag):
    api = Twython(settings.TWITTER_APP_KEY, access_token=settings.TWITTER_ACCESS_TOKEN)
    r = api.search(q="#" + hashtag, count=25, result_type="mixed")
    # find all hashtags in tweets retrieved
    hashtags_list = set(
        sum(
            [[e["text"] for e in r["statuses"][k]["entities"]["hashtags"]] for k in range(len(r["statuses"]))],
            [],
        )
    )
    hashtags = [{"id": h, "text": "#" + h} for h in hashtags_list]
    # find all mentions in tweets retrieved
    mention_list = set(
        sum(
            [[e["screen_name"] for e in r["statuses"][k]["entities"]["user_mentions"]] for k in range(len(r["statuses"]))],
            [],
        )
    )
    # add users
    users = set([r["statuses"][k]["user"]["screen_name"] for k in range(len(r["statuses"]))]).union(mention_list)
    mentions = [{"id": m, "text": "@" + m} for m in users]
    return render(
        request,
        "collect/hashtag.html",
        {
            "hashtag_data": r,
            "hashtags": json.dumps(hashtags),
            "mentions": json.dumps(mentions),
        },
    )


class AnnotateRunView(UpdateView):
    model = GraphMetadata
    template_name = "edit.html"
    success_message = "Success."
    fields = ("info",)

    def get_success_url(self):
        collect_id = self.kwargs["collect_id"]
        success_url = reverse("collect:list_runs", kwargs={"collect_id": collect_id})
        return success_url

    def get_form(self, form_class=None):
        form = super(AnnotateRunView, self).get_form(form_class)
        return _configure_node_form(form)

    def get_context_data(self, **kwargs):
        context = super(AnnotateRunView, self).get_context_data(**kwargs)
        context["title"] = "Run information"
        context["action"] = "Update"
        return context

    def get_object(self, queryset=None):
        """
        Check the logged in user is the owner of the object or 404
        """
        collect = get_object_or_404(DataCollection, id=self.kwargs["collect_id"])
        if collect.owner != self.request.user:
            raise PermissionDenied()
        run = DataCollectionRun.objects.get(pk=self.kwargs["collect_run_id"])
        return run.graphmetadata


class RenameRunView(UpdateView):
    model = GraphMetadata
    template_name = "edit.html"
    success_message = "Success."
    fields = ("name",)

    def get_success_url(self):
        collect_id = self.kwargs["collect_id"]
        success_url = reverse("collect:list_runs", kwargs={"collect_id": collect_id})
        return success_url

    def get_form(self, form_class=None):
        form = super(RenameRunView, self).get_form(form_class)
        return _configure_node_form(form)

    def get_context_data(self, **kwargs):
        context = super(RenameRunView, self).get_context_data(**kwargs)
        context["title"] = "Run name"
        context["action"] = "Update"
        return context

    def get_object(self, queryset=None):
        """
        Check the logged in user is the owner of the object or 404
        """
        collect = get_object_or_404(DataCollection, id=self.kwargs["collect_id"])
        if collect.owner != self.request.user:
            raise PermissionDenied()
        run = DataCollectionRun.objects.get(pk=self.kwargs["collect_run_id"])
        return run.graphmetadata


class TwitterCollectViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing or retrieving collections.
    """

    def list(self, request):
        queryset = TwitterDataCollection.objects.filter(owner=request.user).annotate(
            num_initial_nodes=Count("initialnode", distinct=True),
            num_blocked_nodes=Count("userblacklist", distinct=True),
            num_runs=Count("datacollectionrun", distinct=True),
        )
        serializer = TwitterCollectionSummarySerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = TwitterDataCollection.objects.all()
        collection = get_object_or_404(queryset, pk=pk)
        if collection.owner != request.user:
            raise PermissionDenied()
        serializer = TwitterCollectionSerializer(collection)
        return Response(serializer.data)


class DataCollectionRunViewSet(viewsets.ViewSet):
    def retrieve(self, request, pk=None):
        queryset = DataCollectionRun.objects.all()
        run = get_object_or_404(queryset, pk=pk)
        collect = get_object_or_404(DataCollection, id=run.collection.id)
        if collect.owner != request.user:
            raise PermissionDenied()
        serializer = DataCollectionRunDetailSerializer(run)
        return Response(serializer.data)
