import json
import io
import gzip
import networkx as nx
import numpy as np
from base64 import b64encode
from fa2 import ForceAtlas2
from collections import defaultdict


def build_hashtags_list(graph):
    hashtags = set()
    node_hashtags = nx.get_node_attributes(graph, "all_hashtags")
    for n in node_hashtags:
        tmp = json.loads(node_hashtags[n].replace("'", '"'))
        hashtags = hashtags.union(tmp.keys())
    return list(hashtags)


def build_cluster_info(graph, clusters, cluster_lexi):
    info = {}
    communities = defaultdict(list)
    for k, v in clusters.items():
        communities[v].append(k)
    for k in communities.keys():  # sort by decreasing indegree
        communities[k] = sorted(communities[k], key=graph.in_degree(), reverse=True)
        sg = nx.subgraph(graph, communities[k])
        info[str(k)] = {  # bson imposes string keys
            "members": communities[k],
            "size": len(communities[k]),
            "density": nx.density(sg),
            "weightedSize": sg.size(weight="weight"),
            "lexical": cluster_lexi[str(k)],
        }
    return info


def perform_layout(graph):
    scale = 400
    pos = np.random.rand(graph.number_of_nodes(), 2) * scale - 0.5 * scale
    posd = {n: pos[p] for (n, p) in zip(graph.nodes(), range(graph.number_of_nodes()))}
    forceatlas2 = ForceAtlas2(
        # Behavior alternatives
        outboundAttractionDistribution=False,  # Dissuade hubs
        linLogMode=False,  # NOT IMPLEMENTED
        adjustSizes=False,  # Prevent overlap (NOT IMPLEMENTED)
        edgeWeightInfluence=0.1,
        # Performance
        jitterTolerance=1.0,  # Tolerance
        barnesHutOptimize=True,
        barnesHutTheta=0.5,
        multiThreaded=False,  # NOT IMPLEMENTED
        # Tuning
        scalingRatio=10.0,
        strongGravityMode=False,
        gravity=1.0,
        # Log
        verbose=False,
    )
    r = forceatlas2.forceatlas2_networkx_layout(graph.to_undirected(), pos=posd, iterations=3000)
    posattr_fa2 = {n: {"viz": {"position": {"x": r[n][0], "y": r[n][1], "z": 0}}} for n in graph.nodes()}
    nx.set_node_attributes(graph, posattr_fa2)
    with io.BytesIO() as f:
        with gzip.GzipFile(mode="w", fileobj=f) as zf:
            nx.write_gexf(graph, zf, prettyprint=False)
        return b64encode(f.getvalue()).decode("ascii")
