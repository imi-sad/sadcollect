from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from collect.models import DataCollection


class LoginTestCase(TestCase):
    def setUp(self):
        User.objects.create_user("user", email="user@test.com", password="PassW0rD")

    def test_login(self):
        # First check for the default behavior
        response = self.client.get("/collect/")
        self.assertRedirects(response, "/users/login/?next=/collect/")

    def test_user_login(self):
        response = self.client.login(username="user", password="PassW0rD")
        self.assertTrue(response)
        response = self.client.login(username="user", password="Wrongpass")
        self.assertFalse(response)


class CollectViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user("user", email="user@test.com", password="PassW0rD")
        self.user2 = User.objects.create_user("user2", email="user2@test.com", password="pAssWorD")
        self.client.login(username="user", password="PassW0rD")

    def test_empty_collections(self):
        DataCollection.objects.create(
            name="test_u2",
            source="twitter",
            depth=2,
            sampling_probability=10,
            collect_by="coreball",
            owner_id=self.user2.pk,
        )
        response = self.client.get(reverse("collect:home"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No data collection for current user.")
        self.assertQuerysetEqual(response.context["lst_collect"], [])

    def test_one_collection(self):
        DataCollection.objects.create(
            name="test",
            source="twitter",
            depth=3,
            sampling_probability=15,
            collect_by="fireball",
            owner_id=self.user.pk,
        )
        response = self.client.get(reverse("collect:home"))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context["lst_collect"], ["<DataCollection: test>"])


class CollectViewDetailTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user("user", email="user@test.com", password="PassW0rD")
        self.user2 = User.objects.create_user("user2", email="user2@test.com", password="pAssWorD")
        self.coll_user = DataCollection.objects.create(
            name="test",
            source="twitter",
            depth=3,
            sampling_probability=15,
            collect_by="fireball",
            owner_id=self.user.pk,
        )
        self.coll_user2 = DataCollection.objects.create(
            name="test_u2",
            source="twitter",
            depth=2,
            sampling_probability=10,
            collect_by="coreball",
            owner_id=self.user2.pk,
        )
        self.client.login(username="user", password="PassW0rD")

    def test_list_runs(self):
        response = self.client.get(reverse("collect:list_runs", args=(self.coll_user2.pk,)))
        self.assertEqual(response.status_code, 403)
        response = self.client.get(reverse("collect:list_runs", args=(self.coll_user.pk,)))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context["lst_collect"], ["<DataCollection: test>"])
        self.assertQuerysetEqual(response.context["datacollectruns"], [])
        response = self.client.get(reverse("collect:list_runs", args=(1234,)))
        self.assertEqual(response.status_code, 404)

    def test_start_collect(self):
        response = self.client.get(reverse("collect:start_collect", args=(self.coll_user2.pk,)))
        self.assertEqual(response.status_code, 403)
        response = self.client.get(reverse("collect:start_collect", args=(1234,)))
        self.assertEqual(response.status_code, 404)
