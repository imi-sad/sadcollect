import uuid
from django.db import models
from collect.models import DataCollection, DataCollectionRun


class GraphMetadata(models.Model):
    run = models.OneToOneField(DataCollectionRun, on_delete=models.CASCADE)
    number_nodes = models.IntegerField()
    number_edges = models.IntegerField()
    info = models.TextField(default="")
    name = models.CharField(default="", max_length=100)
