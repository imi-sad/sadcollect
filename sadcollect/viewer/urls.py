from django.urls import path
from . import views

app_name = "viewer"
urlpatterns = [
    path("", views.home, name="home"),
    path(
        "graph-layout-data/<uuid:collect_run_id>",
        views.get_graph_data_layout,
        name="get_graph_data_layout",
    ),
    path("display-graph/<uuid:collect_run_id>", views.display_graph, name="display_graph"),
    path("export-gexf/<uuid:collect_run_id>", views.export_gexf, name="export_gexf"),
    path("graph-gexf/<uuid:collect_run_id>", views.get_graph_gexf, name="graph_gexf"),
    path(
        "hashtags-timeline/<uuid:collect_run_id>",
        views.hashtags_timeline,
        name="hashtags_timeline",
    ),
    path(
        "hashtags-timeline/<uuid:collect_run_id>/<int:max_days>",
        views.hashtags_timeline,
        name="hashtags_timeline",
    ),
    path(
        "hashtags-timeline/<uuid:collect_run_id>/<int:max_days>/<int:resampling_step_hours>",
        views.hashtags_timeline,
        name="hashtags_timeline",
    ),
]
