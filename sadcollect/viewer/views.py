import gzip
import json
from io import BytesIO
from base64 import b64decode
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.authentication import (
    SessionAuthentication,
    BasicAuthentication,
    TokenAuthentication,
)
from rest_framework.permissions import IsAuthenticated
from collect.models import DataCollection, DataCollectionRun
import pandas as pd
import pymongo
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from urllib.parse import quote_plus
from datetime import datetime, timedelta
from django.views.decorators.cache import cache_page
import logging

logger = logging.getLogger()


@login_required
def home(request):
    return render(request, "viewer/home.html")


def _get_graph_result_data(collect_run, layout=True):
    task_mongo_cfg = settings.CELERY_MONGODB_BACKEND_SETTINGS
    mongo_url = settings.CELERY_RESULT_BACKEND
    if "collect.tasks.graph_layout_fa2" in collect_run.chain:
        collect_run_id = collect_run.chain["collect.tasks.graph_layout_fa2"]
    else:
        collect_run_id = collect_run.id
    client = MongoClient(mongo_url)
    collection = client[task_mongo_cfg["database"]][task_mongo_cfg["taskmeta_collection"]]
    res = collection.find_one({"_id": str(collect_run_id)})

    if not layout:
        return res
    # first entry is for compatibility
    if "graph_layout" in res["result"]:
        return res["result"]["graph_layout"]

    return res["result"]["graphLayout"]


@api_view(["GET"])
@cache_page(60 * 20)
def get_graph_data_layout(request, collect_run_id):
    collect_run = get_object_or_404(DataCollectionRun, id=collect_run_id)
    collect = get_object_or_404(DataCollection, id=collect_run.collection.id)
    if collect.owner != request.user:
        raise PermissionDenied()
    return JsonResponse(_get_graph_result_data(collect_run))


@login_required
def display_graph(request, collect_run_id):
    collect_run = get_object_or_404(DataCollectionRun, id=collect_run_id)
    collect = get_object_or_404(DataCollection, id=collect_run.collection.id)
    sadview_sentry_dsn = settings.SADVIEW_SENTRY_DSN
    load_sentry = len(sadview_sentry_dsn) > 0
    if collect.owner != request.user:
        raise PermissionDenied()
    return render(
        request,
        "viewer/display_graph.html",
        context={"load_sentry": load_sentry, "sadview_sentry_dsn": sadview_sentry_dsn},
    )


@api_view(["GET"])
def get_graph_gexf(request, collect_run_id):
    collect_run = get_object_or_404(DataCollectionRun, id=collect_run_id)
    data = _get_graph_result_data(collect_run)
    with BytesIO(b64decode(data["compressedGraph"])) as f:
        with gzip.GzipFile(mode="r", fileobj=f) as zf:
            graph_str = zf.read().decode("utf-8")
    return graph_str


@login_required
def export_gexf(request, collect_run_id):
    collect_run = get_object_or_404(DataCollectionRun, id=collect_run_id)
    data = _get_graph_result_data(collect_run)
    with BytesIO(b64decode(data["compressedGraph"])) as f:
        with gzip.GzipFile(mode="r", fileobj=f) as zf:
            graph_str = zf.read().decode("utf-8")
    filename = "{}.gexf".format(collect_run_id)

    response = HttpResponse(graph_str, content_type="application/gexf")
    response["Content-Disposition"] = 'attachment; filename="' + filename + '"'
    return response


def extract_hashtags(tw_obj):
    tw_clean = {
        "id": tw_obj["_id"],
        "user": tw_obj["user"]["screen_name"],
        "created_date": tw_obj["created_date"],
        "hashtags": [k.get("tag", "") + k.get("text", "") for k in tw_obj["entities"]["hashtags"]],
    }
    return tw_clean


@api_view(["GET"])
@authentication_classes([SessionAuthentication, BasicAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated])
@cache_page(60 * 60 * 24)
def hashtags_timeline(request, collect_run_id, max_days=30, resampling_step_hours=4):
    collect_run = get_object_or_404(DataCollectionRun, id=collect_run_id)
    data = _get_graph_result_data(collect_run, layout=False)
    # MongoDB cfg
    cfg = settings.TWEETS_MONGODB_CFG
    mongo_url = "mongodb://{}:{}@{}/{}?authSource={}".format(
        quote_plus(cfg["user"]),
        quote_plus(cfg["password"]),
        cfg["host"],
        cfg["dbname"],
        cfg["dbname"],
    )
    try:
        client = MongoClient(mongo_url, cfg["port"])
        tweets_db = client[cfg["dbname"]][cfg["tweets_collection"]]
    except ConnectionFailure:
        logger.error("Server not available {}@{}:{}".format(cfg["user"], cfg["host"], cfg["port"]))

    users = data["result"]["max_tweet_id"].keys()
    tweets = []
    for k in users:
        # retrieve tweets from user that contain hashtags and fall within the proper date range
        r = list(
            tweets_db.find(
                {
                    "user.screen_name": k,
                    "_id": {"$lte": data["result"]["max_tweet_id"][k]},
                    "entities.hashtags": {"$exists": True},
                    "created_date": {"$gte": datetime.strptime(data["date_done"], "%Y-%m-%dT%H:%M:%S.%f") - timedelta(max_days)},
                },
                # projection needed to speed up query
                projection={
                    "entities.hashtags": True,
                    "created_date": True,
                    "user.screen_name": True,
                },
                limit=100,
                sort=[("_id", pymongo.DESCENDING)],
            )
        )
        tweets += r

    cleaned_data = [extract_hashtags(t) for t in tweets]
    ht_df = pd.DataFrame.from_dict(filter(lambda x: len(x["hashtags"]) > 0, cleaned_data))
    if ht_df.empty:
        return JsonResponse({})
    ht_grp = ht_df.resample("{}h".format(resampling_step_hours), on="created_date").agg(list).drop(columns=["id"])
    ht_grp.index.rename("created_date_rs", inplace=True)
    ht_expl = ht_grp.explode(["user", "hashtags"]).explode("hashtags")
    # hashtags are not case-sensitive -> convert to lower
    ht_expl["hashtags"] = ht_expl["hashtags"].str.lower()

    ht_expl["time_offset"] = (ht_expl.index - ht_expl.index.min()).total_seconds().astype("int")

    # build time data
    time_data = ht_expl[["time_offset"]].reset_index().set_index("time_offset").drop_duplicates()
    time_data["time_offset_rel"] = (time_data.index - time_data.index.min()) / (time_data.index.max() - time_data.index.min())
    # convert datetime column to string s.t. json export is better
    time_data["created_date_rs"] = time_data["created_date_rs"].dt.strftime("%Y-%m-%dT%H:%M:%S")

    # get hashtags per user/time
    ht_timeline = ht_expl.groupby(["user", "time_offset", "hashtags"]).size()
    timeline_df = ht_timeline.to_frame().rename(columns={0: "count"}).reset_index()

    timeline_dict = {
        k: g.pivot_table("count", "time_offset", "user", fill_value=0).to_dict(orient="index") for k, g in timeline_df.groupby("hashtags")
    }
    return JsonResponse({"time": time_data.to_dict(orient="index"), "hashtags": timeline_dict})
