"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("collect/", include("collect.urls")),
    path("viewer/", include("viewer.urls")),
    path("users/", include("users.urls")),
    path("", RedirectView.as_view(url="collect/", permanent=True)),
    path("select2/", include("django_select2.urls")),
    re_path(r"^celery-progress/", include("celery_progress.urls")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
