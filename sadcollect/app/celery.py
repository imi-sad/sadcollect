import os
from celery import Celery
from kombu.serialization import register, registry


try:
    from app.celery_sentry import *
except ImportError:
    raise

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
app = Celery("app")
app.config_from_object("django.conf:settings", namespace="CELERY")

# define custom serializer s.t. result objects are stored properly into mongodb
register(
    "bson",
    lambda obj: obj,
    lambda obj: obj,
    content_type="application/mongo-json",
    content_encoding="utf-8",
)
app.conf.result_serializer = "bson"
registry.enable("bson")


app.autodiscover_tasks()
