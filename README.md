# #SADcollect

This django application is one of the outcomes of the projects [#SAD](https://www.media-initiative.ch/project/social-network-architectures-of-disinformation-sad/) and  [#SAD2](https://www.media-initiative.ch/project/sad-ii/), which are a collaboration between [LTS2 (EPFL)](https://lts2.epfl.ch), [AJM (UniNE)](https://unine.ch/ajm/) and [RTS](https://rts.ch), under the financing of the [Initiative for Media Innovation (IMI)](https://www.media-initiative.ch/).

The goal of this application is to allow automated exploration of Twitter, by sampling the neighbourhood around a restricted number of initial accounts and following the connections those accounts have (i.e. who they retweet and mention).

You can read about the exploration technique in [our paper](https://www.mdpi.com/1999-4893/13/11/275/pdf) and the associated [python implementation](https://github.com/epfl-lts2/spikexplore).

# Installation
## Obtain a twitter API key
In order to use the Twitter API, you have to first register as developer and get the necessary keys. 
Follow [the instructions](https://developer.twitter.com/en/docs/twitter-api/getting-started/getting-access-to-the-twitter-api).

## Running with docker-compose
The simplest way of running all the components is to use `docker-compose`. Check the [installation instructions](https://docs.docker.com/compose/install/). 

- Before running You will need to customize the `sadcollect/app/settingsLocal.py`. A basic example that only misses your Twitter API keys
is already supplied with the code under the name `sadcollet/app/settingsLocal.py.example`. Rename the
file, and fill the missing values. If you run with the supplied `docker-compose.yml` file,
only the Twitter API information is missing. 
- start all necessary services with `docker compose up`
- if this is the first time you run the app:
  - you need to create a super user with the following command 
  `docker compose run django python manage.py createsuperuser`
  - Open your browser and go to `http://localhost:8000/admin`. Login with the superuser credentials 
entered at the previous step. You can now create a regular user.
- Go to `http://localhost:8000` and login to the app

## Installation on a dedicated instance

This is obviously more complicated, and will not be described in details as setup can 
be quite different depending on your local configuration. Here are the requirements
- a web server (e.g. Apache, gunicorn, etc.) able to run a django app, with a suitable database (we used postgresql)
- a mongodb database to store the collected tweets and to be used as celery backend
- memcached

You need to install the necessary python packages in a virtual/conda environment: 
`pip install -r sadcollect/data/pip-reqs.txt`.
Create a `sadcollect/app/settingsLocal.py` file and fill it using the values that match your setup 
(the `sadcollet/app/settingsLocal.py.example` might help). Once properly configured, you need to 
do the initial setup with `python manage.py migrate` (to create the DB structure), create the superuser with `python manage.py createsuperuser`