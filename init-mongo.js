db.createUser({
  user: 'dock_sadcollect',
  pwd: 'secret_password_uncrackable',
  roles: [
    {
      role: 'readWrite',
      db: 'sadcollect'
    }
  ]
})
